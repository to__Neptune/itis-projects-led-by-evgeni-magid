\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{float}
\usepackage{adjustbox}
\usepackage{hyperref}
\usepackage{makecell}

\begin{document}

\title{Comparative Analysis of Mobile Robot Wheels Design\\
}

\author{\IEEEauthorblockN{Ksenia Shabalina, Artur Sagitov and Evgeni Magid}
\IEEEauthorblockA{\textit{Intelligent Robotics Department} \\
\textit{Higher School of Information Technology and Information Systems}\\
Kazan, Russian Federation \\
ks.shabalina@it.kfu.ru, sagitov@it.kfu.ru, magid@it.kfu.ru}
}

\maketitle

\begin{abstract}
This paper considers a mobile robot platform design with a special emphasis on wheeled robots. Each platform is designed for a set of specific tasks and thus is supposed to work in previously known conditions of its environment. A robotic system could be constructed as a holonomic or a non-holonomic system, which directly correlates with a type of its wheels.  In this work we compare different types of mobile robot wheels, including conventional wheels, universal omnidirectional wheels, Mecanum wheels, caster wheels, and steering standard wheels, and analyze the best scenario of design application. This paper shares our experience in selecting wheeled platform design and could be considered as a brief practical guideline for beginners in mobile robot platform design.
\end{abstract}

\begin{IEEEkeywords}
Mobile robotics, comparison, omni drive, robot platform, robot design
\end{IEEEkeywords}

\section{Introduction}
\label{sec:intro}
Robotics field is rapidly developing, and robots are gradually becoming an important part of everyday life of humankind. Besides such classical areas as industrial \cite{sciavicco2012modelling}, medical and rehabilitation robotics \cite{nunes2011cable}, which mainly use various manipulators with certain task-oriented end effectors while both a task and an operational environment could be a-priori described rather precisely, robotics gradually takes its place in less predictable fields such as, for example, human robot interaction \cite{gomez2013social}, exploration \cite{vokhmintsev2016simultaneous} and urban search and rescue \cite{magid2010static}. In nowadays robots are applied not only in factory settings, surgery operational rooms and warehouses, but interact with a human on a daily basis inside usual premises, including offices, hotels, corridors of hospitals and living apartments. Often in such settings mobile robots require a capability to operate in a confined space \cite{kim2010practical}. One of the natural selections for confined space operating in human-oriented environments today are bipedal robots, but their development and especially dynamically stable locomotion algorithms, fall detection and management \cite{magid2017towards} require significantly more efforts than a development of conventional wheeled robots that could move freely in confined spaces.

Majority of modern mobile robots are constructed using wheels. However, this requires to consider maneuverability and controllability of a robot within its target environment at the design stage. Among the variety of options for number of wheels, their type and configuration within a mobile robot platform base a designer should select a most suitable one. To guarantee good maneuverability of a mobile robot in a narrow space, omnidirectional wheeled solutions should be considered. 

This paper explores a question of a wheeled mobile robot base design from a locomotion point of view tackling the choice of wheels and their configuration. We consider holonomic and non-holonomic robots and the corresponding types of wheels: conventional wheels, steering wheels, caster wheels, universal wheels, and Mecanum wheels. Next, we discuss particular strengths and drawbacks of each type, which provides useful technical parameters of each wheel type and could serve as a recommendation for a robot designer while selecting mobile robot locomotion modes. We do not pretend to cover all existing models and modifications of wheels (e.g., we do not deepen into multiple modifications of Mecanum wheels \cite{ramirez2010modified} or the newly created omnidirectional wheels \cite{ren2013analysis}) within this paper due to space limitations, but we do demonstrate the most popular and widely used type of wheels.

The rest of the paper is organized as follows. Section \ref{sec:holo_nonholo_sys} reviews typical wheels of a mobile robot with a brief overview of their main advantages and disadvantages. Section \ref{sec:analysis} describes optional wheel configurations within a mobile robot base. Section \ref{sec:comparison} provides a comparative analysis of these typical wheels, followed by a discussion in Section \ref{sec:discussion}. Finally, we conclude in Section \ref{sec:conclusion}.


\section{Holonomic and Non-holonomic systems}
\label{sec:holo_nonholo_sys}
First we need to distinguish between holonomic and non-holonomic systems. A system is called holonomic if the number of controlled degrees of freedom (DoF) is equal to the total number of DoF \cite{siegwart2011introduction}. Therefore, the robot is a non-holonomic system if the number of its controlled degrees is less than the total number of DoF. The property of holonomicity directly depends on the type of robot wheels. Consider a non-holonomic car-like robot, which is typically an Ackerman wheeled system that cannot move freely in any direction. At the same time a robot, which is equipped with omnidirectional wheels, becomes holonomic. This section describes holonomic and non-holonomic systems in terms of wheels’ usage as well as strengths and weakness of each type of wheels. In this paper we look at differential drive robots and Ackerman drive robots as representatives of non-holonomic systems, and consider caster wheels (including a caster ball), universal wheels, Mecanum wheels, omnidirectional wheels) as the ones to support holonomic systems examples.

\subsection{Conventional Wheel}
\label{subsec:conv_wheel}
One of the examples of a non-holonomic system is a differential drive wheeled robot with conventional wheels. A conventional wheel (Fig. \ref{fig:wheel_types}, a) is widely used in all engineering areas due to its simplicity and functionality, which is limited to providing forward and backward rotation of a wheel. For a differential drive robot conventional wheels allow robot rotation when different rotation speeds and/or rotation direction are applied for its conventional wheels. 

\subsection{Steering Wheel}
\label{subsec:steering_wheel}
At a first glance a steering wheel may look completely like a conventional wheel , but it has a different mechanical structure. The term “steering” means not just a wheel, but a certain steering mechanism, which allows a conventional wheel to rotate around its vertical axis. For this purpose, a mechanism uses a steering motor to control a movement direction (i.e., a rotation around the vertical axis) of a wheel, and a driving motor to provide forward and backward locomotion. This way, the same physical wheel could be used in the role of a conventional wheel or, by attaching a steering mechanism, it could be transformed into a steering wheel. 

\subsection{Caster Wheel}
\label{subsec:caster_wheel}
Another type of a wheel is a caster (or sometimes called \textit{castor}) wheel (Fig. \ref{fig:wheel_types}, b-2). Caster wheels have a wide application not only in robotics, but are also used in service and medical equipment, manufacturing etc. Using caster wheels helps to achieve a near-omnidirectional mobility of a mobile robot or any other mechanical vehicle. 
Some manufacturers divide caster wheels strictly into two categories: rigid wheels and swivel wheels. In the case of a rigid wheel, the wheel can rotate only forward and backward. For a swivel wheel category, the wheel can passively rotate 360 degrees with regard to the vertical axis as well as to rotate forward and backward, providing a free movement of the wheel. A special type of a caster wheel is a ball caster wheel (Fig. \ref{fig:wheel_types}, b-1), which provides a free motion in all directions due to the use of a passive sphere in a role of a wheel. A ball caster wheels are widely used as additional passive wheels with other active driving wheels, e.g., it could be used to provide an additional ground-contact (rolling) point within a differential drive based platform.


\subsection{Universal Omni Wheel}
\label{subsec:univ_wheel}
The basic idea of an omni wheel is a combination of a main active wheel and passive freely rotating rollers. The active wheel and the rollers have their own rotation axes and in the case of universal wheels, axes of passive rollers are orthogonal to a main wheel axis~\cite{doroftei2007omnidirectional}. While an active wheel is rotating in clockwise or counterclockwise direction with respect to its rotation axis, combining active rotation of several active wheels with passively rotating rollers allows supporting locomotion almost in any direction. 
Typically, rollers have a cylindrical shape ( \ref{fig:wheel_types}, c) and their number may vary.
Even though omnidirectional wheels provide free locomotion in a 2D space, they have a number of disadvantages (e.g., inefficiency at a dirty surface) which are considered in the next Section.

\subsection{Mecanum Wheel}
\label{subsec:mecanum_wheel}
These wheels are similar to a universal wheel construction except that rollers are mounted with their axis at an angle of 45 degrees relatively to an axis of an active wheel base (Fig. \ref{fig:wheel_types}, d). It was first developed by a company “Mecanum AB” in 1973 by Bengt Ilon engineer \cite{ilon1973directionally}. Since a mecanum wheel design is complex, manufacturing cost is greater as compared to universal wheels. These wheels are capable to roll about an axis of an active wheel (i.e., a base wheel) and also about axis of rollers at an angle of 45 degrees. Applying different velocities to each wheel a robot can moves in any direction; classic Ilon wheels have 3 Degree of freedom: wheel rotation, roller rotation, and rotational slip about the vertical axis passing through the point of contact \cite{doroftei2007omnidirectional}. Thus, Mecanum wheels can move in a desired direction, allow a diagonal movement with regard to heading direction and a rotation around a robot vertical axis in place. More details about strengths and drawbacks of wheel be considered in the next sections of the paper.

\begin{figure} [b]
	\centering
	\includegraphics[width=220px]{img/wheels.jpg}
	\caption{Wheel types: a --- a conventional / steering wheel, b-1 ---  a caster ball wheel, b-2 --- a caster wheel, c --- a universal wheel, d --- a Mecanum wheel.} \label{fig:wheel_types}
\end{figure}

\section{Analysis of Wheel Configuration}
\label{sec:analysis}
In this section we demonstrate a number of examples for possible wheel configurations.  
While there are no limitations for a number of wheels or their placement within a mobile robot base, the following schemes reflect most broadly used  by robotics community (due to an optimal design) configurations.


\subsection{Conventional Wheel and Caster Wheel}
\label{subsec:conv_caster}
Caster wheels are used as passive wheels and they can be applied together with other wheels to reach omnidirectional mobility. One of the popular configurations for using such wheels is a differential drive robot; conventional wheels are used as active wheels and a caster wheel adds stability to a robot (providing a triangular support polygon) and is used to allow free rotation (Fig. \ref{fig:diff}, a).

\begin{figure} [b]
	\centering
	\includegraphics[width=80px]{img/diff_mec.png}
	\caption{Configuration of differential drive robot with conventional wheels and a passive caster wheel.} \label{fig:diff}
\end{figure}

\subsection{Steering Wheel}
\label{subsec:steering}

% начиная отсюда - с Артуром переделывайте, не годится
% мысли скачут, все несвязано!

Steering wheels name comes from their steering mechanism, which acts through a drivetrain mechanism of a robot. Thus, a steering motor controls a steering direction (to the left or to the right) of the wheels and this way a motion direction, while a drive motor transfers a torque to the wheels to provide toward and backward movement.
%Conventional Steering же?? 
Steering wheels are typically used in four-wheeled (e.g., car-like robots, Fig. \ref{fig:steering} a, b, c), three-wheeled (e.g., triangular cart-like platforms, Fig. \ref{fig:steering}, d) and two-wheeled configurations (e.g., bicycle). For a three-wheeled configuration it is popular to combine two active steering wheels with a single caster wheel in a role of a passive wheel (Fig. \ref{fig:steering}, d)

\begin{figure} [t]
	\centering
	\includegraphics[width=220px]{img/steering.png}
	\caption{Steering wheels configurations: a --- a generic view of configuration, b --- an Ackerman drive configuration, c --- a near omni drive configuration, d --- steering wheels and a caster passive wheel.} \label{fig:steering}
\end{figure}

\subsection{Universal Omni Wheel}
\label{sec:univ}
The configuration can be optimally composed using three of four wheels. Three universal wheels could be mounted on a triangular platform with their axes being inclined by 120 degrees relative to each other (Fig. \ref{fig:universal}a and Fig. \ref{fig:example_mecanum}, left). 

In four-wheeled design two configurations are typical:
\begin{enumerate}
	\item Wheels are located as shown in Fig. \ref{fig:universal} (b), and the angle between wheels are 90 degrees
	\item Wheels are located at corners of a square platform and their axes are inclined by 90 degrees relative to each other (Fig. \ref{fig:universal}, c) \cite{kanjanawanishkul2015omnidirectional}
\end{enumerate}

Examples of such universal wheels’ construction are presented in \cite{ismael2016analysis} \cite{rojas2006holonomic}.
% arrangement? mechanical construction?

\begin{figure} [b]
	\centering
	\includegraphics[width=220px]{img/uni_config.png}
	\caption{Universal wheel configurations.} \label{fig:universal}
\end{figure}

\subsection{Mecanum Wheel}
\label{subsec:mecanum}
If we consider Mecanum omnidirectional wheels, there is one optimal design for their location at the robot rectangular or square platform. The Mecanum wheels are located in style of car-like robots (Fig. \ref{fig:mecanum}, Fig. \ref{fig:example_mecanum} right): they mounted inline to each other; usually robots use four wheels, but this is not a strong limitation \cite{wu2017optimal},\cite{sarmento2017remote}, \cite{yip2014development}, \cite{wu2017design}, \cite{salih2006designing} \cite{tapia2016autonomous}.


\begin{figure} [h]
	\centering
	\includegraphics[width=80px]{img/mecanum.png}
	\caption{Configuration of Mecanum wheels.} \label{fig:mecanum}
\end{figure}

\begin{figure} [b]
	\centering
	\includegraphics[width=100px]{img/universal.jpg}
	\hspace{.2in}
	\includegraphics[width=100px]{img/mecanum.jpg}
	\caption{Example of omni drive robot platform using three universal wheels \cite{ismael2016analysis}  (left) and four Mecanum wheels \cite{salih2006designing} (right).} \label{fig:example_mecanum}
\end{figure}

\section{Wheels Comparison}
\label{sec:comparison}
In this Section we compared certain type of wheels and make analysis of strengths and drawbacks of each type of wheels. For comparison we will use following type of wheels: conventional wheels, universal wheels, Mecanum, steering wheels and caster wheels. For comparison we made two assumptions:
As mentioned above, while physically conventional wheels and steering wheels are the same, they differ in mechanical parameters 
We did not divide caster wheels into caster wheel and ball caster wheel (spherical) and considered their parameters together.

For our comparison we selected the general mechanical properties: manufacturing complexity \cite{doroftei2007omnidirectional}, sensitivity to a rough surface, sensitivity to small (extraneous) objects on the surface \cite{kanjanawanishkul2015omnidirectional} \cite{wu2017design}, possible wheels’ configuration (guided by configurations discussed earlier) \cite{kanjanawanishkul2015omnidirectional}, minimal required quantity of wheels (minimal quantity of sufficient robot configuration) and degrees of freedom (DoF). For the second comparison we selected two technical parameters of wheels and investigated the approximate values using technical information in on websites of wheel manufacturers and their suppliers \cite{Andy}, \cite{ActiveRob}, \cite{Spark}, \cite{RobShop}. For convenience, we separated the results in three tables \ref{tab1}-\ref{tab3}.


\begin{table} [b]
	\caption{General Parameters of wheels.}\label{tab1} \centering 
	\resizebox{\columnwidth}{!}{
		
		\begin{tabular}{|c|c|c|c|}
			\hline
			\textbf{\thead{Type of \\ wheels}} & \textbf{\thead{Manufacturing \\complexity}} & \textbf{\thead{Sensitivity to a \\ rough surface}} & \textbf{\thead{Sensitivity to small \\ (extraneous)  objects \\ on the surface}}\\
			\hline
			Conventional wheel &  Low & Low &  Low\\ 
			\hline
			Steering wheel &  Low & Low & Low \\
			\hline
			Caster wheel & Low & Low & Low / High for caster ball\\
			\hline
			Universal wheel & Medium & High & High \\
			\hline
			Mecanum wheel & High & High & High\\
			\hline
		\end{tabular}
		
	}
\end{table}

\begin{table} [t]
	\caption{Mechanical parameters of wheels and configurations.}\label{tab2}  \centering 
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{|c|c|c|c|}
			\hline
			\textbf{\thead{Type of \\ wheels}} & \textbf{\thead{Possible wheels \\configuration}}& \textbf{\thead{Minimal required \\quantity of wheels}}
			& \textbf{DoF}\\
			\hline
			Conventional wheel &  2 or more wheels & 2 &  1 \\ 
			\hline
			Steering wheel & \thead {2 (with one different wheel)\\ 3, 4 wheels} & 2 (with one different wheel) & 2 \\
			\hline
			Caster wheel & \thead {1 (As a part of configuration\\ with different wheels)} & \thead {1 (As a part of configuration\\ with different wheels)} & 1, 2, 3\\
			\hline
			Universal wheel & 3 or 4 wheels & 3 & 3 \\
			\hline
			Mecanum wheel & 4 or more wheels & 4 & 3\\
			\hline
		\end{tabular}
	}
\end{table}

\begin{table} [t]
	\caption{Technical Parameters.}\label{tab3}\centering 
		\resizebox{\columnwidth}{!}{
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Type of wheels}&  \textbf{\thead{Minimum \\required size}}& \textbf{Maximum load}\\
		\hline
		\thead{Conventional wheel \\ and steering wheel} &  50,8 mm & Up to 40-60 kg \\ 
		\hline
		Caster wheel & 25,4 mm & 15 kg \\
		\hline
		Universal wheel & 101,6 mm & 2-30 kg \\
		\hline
		Mecanum wheel & 101,6 mm & 4 7-15 kg \\
		\hline
	\end{tabular}
	}
\end{table}

\section{Discussion and Future Work}
\label{sec:discussion}
Table \ref{tab1} presents the results of general parameters of the wheels. In such parameters as sensitivity to floor conditions and manufacturing complexity the simple wheels (conventional, steering, and swivel caster) is the best decision for use. They did not have complex design and almost robust for unideal or rough surface. At the same time, ball caster wheel really has sensitivity to small objects on the floor because the space between the spinning sphere and the “cap” may be clogged by small particles. 

Table \ref{tab2} shows the results of mechanical parameters of wheels and sum up wheels’ configuration. Conventional wheel has only one degree of freedom (moving forward and backward), but requires at least two wheels in case of differential drive robot and actually does not have upper limits. However, conventional wheels cannot provide omnidirectional mobility. Steering wheels have two degrees of freedom (moving forward and backward, rotation around vertical axis) and can be used in several configurations: two wheels (with one passive caster), three wheels (as synchronized wheels), and four wheels as car-like robot. Caster wheel consist of three types and each type has its one degree of freedom value. Caster ball has 3 degrees of freedom due its design, but can be implement only as passive wheel. Swivel caster has two DoF and also used as passive wheel. The third type of caster (rigid wheel) has only one DoF. The omnidirectional wheels have 3 DoF; universal wheels provide configuration of three or four wheels, the Swedish wheel optimally used as car-like wheels’ position of four wheels.

Table \ref{tab3} contains some technical information about wheels that can be useful for designing the robot model. Each of wheel has its minimal size and maximum load capacity. Again, the conventional wheel has the best load capacity due its simple design, while omnidirectional wheels are more sensitive to maximum load parameter.

As a results of this Section, each type of wheel has its own strengths and drawbacks. Conventional wheels are very reliable and robust. But does not provide freely movement as omnidirectional wheels. Omni wheels are excellent choice to ensure the robot's maneuverability in indoor and narrow space, but the worst for outdoor tasks. Caster wheels are optimal and simple way to give more freely movement classical wheels (conventional and steering). All of the wheels have configurations for designing a mobile platform. However, at the beginning the major question is to determine the work environment for the robot, the task of the robot and approximate robot’s weight for the correct wheel size.

\section{Conclusions}
\label{sec:conclusion}
In this paper we compared certain type of wheels: conventional wheel, steering wheel, caster wheel, Mecanum wheel and universal wheel to facilitate the question of selection the most suitable wheel type and configuration for constructing a mobile platform. We selected mechanical parameters and some technical values to investigate the question of existing strengths and drawback of each type. The general and mechanical parameters are: manufacturing complexity, sensitivity to a rough surface, sensitivity to small (extraneous) objects on the surface, possible wheels’ configuration (guided by configurations discussed earlier), minimal required quantity of wheels (minimal quantity of sufficient robot configuration) and degrees of freedom (DoF). For the second comparison we selected two technical parameters: minimal required size and maximum load capacity. As a result, each type of wheel has its own strengths and drawbacks. Conventional wheels are robust, but does not provide such maneuverability as omnidirectional wheels. However, both omni wheels have complex manufacture design and high sensitivity to floor conditions. Therefore, the first step of designing and selecting the wheels is awareness of further robot’s workspace and application area.



\section*{Acknowledgment}

	\bibliographystyle{IEEEtran}
    \bibliography{2018_DESE}
	
\end{document}
