Readme





RU

Для того, чтобы запустить консольное приложение C++ через терминал в ОС Linux, я предлагаю следующий варинат.

Precondition

1) Установить Notepadqq http://notepadqq.altervista.org/wp/download/ 

2) Создать в ней документ, написать код на C++ (самый простой вариант- вывод:"Hello World")

3) Сохранить документом с разрешением cpp

4) Установить G++ компилятор, введя в терминале : sudo apt-get install g++



Шаги: 

1) Введите пароль учетной записи

2)Введите команду g++ -lm -o output <ИмяВашегоПриложения>.cpp.

--Ничего не должно произойти,если код верный --

3) введите ./output и нажмите Enter 

DONE









ENG

In order to run a C++ cosole application on Linux by the terminal, i propose the folowing variant.

Precondition: 

1)Install Notepadqq  http://notepadqq.altervista.org/wp/download/ 

2)Create a document in Notepadqq,write a simple C++ code (for example - "Hello World")

3) Save the document in ".cpp" format

4) Install the g++ compiler - Enter in terminal command -  sudo apt-get install g ++





Steps.

1) Open terminal

2) Enter a command -- g ++ -lm -o output <ProjectName>.cpp  --



If there are no errors in your code, terminal will not display error messages



3)Enter a command -- ./output and press "Enter"







