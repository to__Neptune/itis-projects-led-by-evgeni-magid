#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <QTextStream>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->horizontalSlider,SIGNAL(valueChanged(int)),ui->progressBar,SLOT(setValue(int)));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_go_clicked()
{
   if ( ui->checkBox->isChecked())


    //ui->label->setText("Meow, clicked");
   // QString a = ui->lineEdit->text();
   // QString b = ui->lineEdit_2->text();
   // int d = ui->lineEdit->text()->;

   QMessageBox::information(this,"CheckBox","yes");
}

void Widget::on_pushButton_clicked()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Open a file",QDir::homePath());
    QMessageBox::information(this,"..",file_name);
}

void Widget::on_pushButton_3_clicked()
{  QString filter = "All File (*.*) ;; Text File (*.txt) ;; XML File (*.xml)";
    QString file_name = QFileDialog::getOpenFileName(this,"open a file",filter);
    QFile file(file_name);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this,"title","file not open");
    }

    QTextStream in(&file);
    QString text = in.readAll();
  ui->plainTextEdit->setPlainText(text);
    file.close();
}
